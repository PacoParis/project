package com.example.hotspots.TEA4;

import android.util.Log;
import android.view.View;

import androidx.test.core.app.ActivityScenario;
//import androidx.test.espresso.intent.Intents;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.hotspots.LoginActivity;
import com.example.hotspots.R;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
//import static androidx.test.espresso.intent.Intents.intended;
//import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LlistarNotificacionsTest {

  private View decorView;

  @Rule
  public ActivityScenarioRule<LoginActivity> activityRule = new ActivityScenarioRule<>(LoginActivity.class);


  @Before
  public void setUp() {
    activityRule.getScenario().onActivity(new ActivityScenario.ActivityAction<LoginActivity>() {
      @Override
      public void perform(LoginActivity activity) {
        decorView = activity.getWindow().getDecorView();
        Log.d("HOTSPOT_DEBUG", "Created view ");
      }
    });
  }

  private void tempsEspera(){
    try{
      Thread.sleep(1000);
    }catch (Exception error){
      error.printStackTrace();
      Log.d("HOTSPOT_DEBUG", "Error in wait ");
    }
  }



  /*@Test
  public void emailMalFormat() {

    tempsEspera();

    //Escriviu el correu electrònic i la contrasenya no vàlids
    onView(ViewMatchers.withId(R.id.edit_email)).perform(typeText("fgmail.com"));
    onView(withId(R.id.edit_password)).perform(typeText("12345"), closeSoftKeyboard());

    //Comproveu si l'usuari té el botó visible
    onView(withId(R.id.button_action)).check(matches(isDisplayed()));

    //Feu clic al botó
    onView(withId(R.id.button_action)).perform(click());

    //Comproveu si es mostra el missatge correcte
    onView(withText("El format email es erroni"))
            .inRoot(withDecorView(not(decorView)))
            .check(matches(isDisplayed()));
  }


  @Test
  public void loginInexistent() {

    tempsEspera();

    //Escriviu el correu electrònic vàlid i la contrasenya no vàlida
    onView(withId(R.id.edit_email)).perform(typeText("francisco@gmail.com"));
    onView(withId(R.id.edit_password)).perform(typeText("PasswdErroni"), closeSoftKeyboard());

    //Comproveu si l'usuari té el botó visible
    onView(withId(R.id.button_action)).check(matches(isDisplayed()));

    //Feu clic al botó
    onView(withId(R.id.button_action)).perform(click());
    tempsEspera();

    //Check if the correct message is displayed
    onView(withText(endsWith("contransenya erronea")))
            .inRoot(withDecorView(not(decorView)))
            .check(matches(isDisplayed()));
  }*/


  @Test
  public void loginOK() {
    tempsEspera();

    //Escriviu el correu electrònic i la contrasenya no vàlids
    onView(withId(R.id.edit_email)).perform(typeText("francisco@gmail.com"));
    onView(withId(R.id.edit_password)).perform(typeText("12345"), closeSoftKeyboard());

    //Comproveu si l'usuari té el botó visible
    onView(withId(R.id.button_action)).check(matches(isDisplayed()));

    //Feu clic al botó
    onView(withId(R.id.button_action)).perform(click());

  }

  @Test
  public void mostraNotificacio(){
    tempsEspera();

    //Comproveu si l'usuari té el botó visible
    onView(withId(R.id.btn_notifications)).check(matches(isDisplayed()));

    //Feu clic al botó
    onView(withId(R.id.btn_notifications)).perform(click());

    tempsEspera();

    onView(withId(R.id.recyclerView)).check(matches(isDisplayed()));

    onView(withId(R.id.recyclerView))
            .perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));

    tempsEspera();

    onView(withId(R.id.scroll)).check(matches(isDisplayed()));

    // Es desplaça fins al final de la descripció
    onView(withId(R.id.scroll)).perform(ViewActions.swipeUp());

    tempsEspera();

  }


}