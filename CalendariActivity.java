package com.example.hotspots;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.hotspots.network.HotspotApi;
import com.example.hotspots.network.NetworkRequest;
import com.squareup.timessquare.CalendarPickerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Clase que permet a l'usuari escollir una o unes dates, (sempre i quan no siguin
 * més de una setmana (6 dies hàbils) per poder fer una reserva.
 * @author Francisco París
 * @version 02.2020
 */
public class CalendariActivity extends AppCompatActivity implements View.OnClickListener {
    protected ArrayList<String> arrayDies = new ArrayList<String>();
    protected Date dataInici = null;
    protected Date dataFi = null;
    protected Date dataIniciSelect = null;
    protected int totalDies = 0;
    protected SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    protected ArrayList<String> llistaDeDies = new ArrayList<>();
    private Button btnCal;
    private Calendar c = Calendar.getInstance();
    private Calendar nextYear = Calendar.getInstance();
    private Date today = new Date();
    private ProgressDialog mProgressDialog;
    private AuthHelper mAuthHelper;

    /*
     * Resposta de trucada per a la reserva de dies en l'oficina
     *  Depenent del missatge de sortida, enviarem un Toast per coneixer si s'ha
     *  fet correctament la reserva
     * */
    private NetworkRequest.Callback<String> datesCallBack = new NetworkRequest.Callback<String>() {
        @Override
        public void onResponse(@NonNull String response) {
            Log.d(Constants.LOG_TAG, "Logout server response1:" + response);

            //Per obtenir el dia que ja està reservat, partim la frase i afegim en un string la resta
            String diaJaReservat[] = response.split("day");
            String dia = diaJaReservat[diaJaReservat.length - 1];

            if (response.contains("User already has a reservation for day")) {
                Toast.makeText(getApplicationContext(), String.format("RECORDA, el dia %s ja tens una reserva. " +
                        "Comprova el llistat", dia), Toast.LENGTH_LONG).show();
            }
            if (response.contains("Booking created")) {
                Toast.makeText(getApplicationContext(), "Reserva efectuada correctament. " +
                        "Gracies per la teva confiança", Toast.LENGTH_LONG).show();
            }
            dismissDialog();
        }

        @Override
        public void onError(String error) {
            Log.d("HOTSPOT_DEBUG2", "Logout server response error:" + error);
            dismissDialog();
        }

        @Override
        public Class<String> type() {
            return String.class;
        }
    };

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, CalendariActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendari);

        mAuthHelper = AuthHelper.getInstance(this);
        mProgressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);

        btnCal = findViewById(R.id.btnCalendar2);
        btnCal.setOnClickListener(this);
        // Afegim el calendari
        nextYear.add(Calendar.YEAR, 1);
        CalendarPickerView datePicker = findViewById(R.id.calendar_grid);
        datePicker.init(today, nextYear.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE)
                .withSelectedDate(today);

        datePicker.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                arrayDies.add(diaSeleccionat(date));
            }

            @Override
            public void onDateUnselected(Date date) {
            }
        });

    }

    /*
     * Fem una reserva si l'Array de datesCallBack no es null
     */
    public void ferReserva() {
        mProgressDialog.setTitle(R.string.msn_dialog_tittle_book);
        mProgressDialog.setMessage("En procès...");
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();

        HotspotApi request = new HotspotApi();
        request.doReserve(mAuthHelper.getIdToken(), llistaDeDies, datesCallBack);
        Log.d("Error en reservar llocs", datesCallBack.toString() + " " + mAuthHelper.getIdToken());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnCalendar2:
                // Sino has escollit cap data, et dona missatge de Toast
                if (arrayDies.size() == 0) {
                    faltaPulsar(arrayDies);
                }
                //Si es clickat una vegada, reservarem desde la data actual fins a la data
                // escollida (arrayDates.get(0))
                if (arrayDies.size() == 1) {
                    totalDies = Integer.parseInt(difDiaActual(arrayDies));

                    for (int i = 0; i < llistaDeDies.size(); i++) {
                        Log.d("arrayDies == 1. Dies en llista: ", llistaDeDies.get(i));
                    }
                }
                //Sino escollim les 2 dates finals escollides
                if (arrayDies.size() > 1) {
                    totalDies = Integer.parseInt(difDiaEscollit(arrayDies.get(arrayDies.size() - 2),
                            arrayDies.get(arrayDies.size() - 1)));

                    for (int i = 0; i < llistaDeDies.size(); i++) {
                        Log.d("arrayDies > 1. Dies en llista: ", llistaDeDies.get(i));
                    }

                }
                // Si el total de dies no superen la setmana, sobre el dialeg preguntant
                // si les dades son correctes
                if (totalDies > 0 && totalDies < 8) {
                    dialeg(llistaDeDies);
                }
                break;
        }
    }

    /*
     * Torna un Toast amb el missatge dient que tens que seleccionar una data almenys
     * @params ArrayList - array dels clics fets al calendari
     * @params Context - la Activitat (Reserves)
     * */
    public void faltaPulsar(ArrayList<String> arrayDates) {
        if (arrayDates.size() == 0) {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.empty_calendar,
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }
    }

    /*
     * Guardem el dia del calendari a marcar.
     * Torna un Toast amb el missatge del dia seleccionat
     * @params Date - el dia seleccionat del calendari
     * @params Context - la Activitat (Reserves)
     * @return String - el dia seleccionat del calendari + 1
     */
    public String diaSeleccionat(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        String selectedDate = cal.get(Calendar.YEAR) + "-" + checkDigit(cal.get(Calendar.MONTH) + 1)
                + "-" + checkDigit(cal.get(Calendar.DAY_OF_MONTH));
        String oneDayMore = cal.get(Calendar.YEAR) + "-" + checkDigit(cal.get(Calendar.MONTH) + 1)
                + "-" + checkDigit(cal.get(Calendar.DAY_OF_MONTH) + 1);

        //Afegeixo 1 dia per afegir el dia seleccionat
        Toast toast = Toast.makeText(this, selectedDate, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();

        return oneDayMore;
    }

    /* Si el numero es mes petit de 9 afegim un 0,
     * Ex: Les 9:0 afegirà un 0 i seràn les 9:00
     * ja que el metode anterior no l'afegeix..
     * @return String - torna un 0 en cas de ser possible
     * */

  public String checkDigit(int number) {
    return number <= 9 ? "0" + number : String.valueOf(number);
  }

    /* Finestra de Dialeg sobre els DIES
     * Sobre una finestra indicant quins dies has seleccionat, en cas de estar d'acord,
     * passa a la següent finestra, en cas contrari, anula les dates reservades i cal
     * escollir de nou les dates
     * @params ArrayList - Dies que has escollit */
    public void dialeg(ArrayList dates) {
        //possem el nostre estil de color en els botons
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setTitle("Reserva de dies:");

        String llistaDies = "";
        for (int i = 0; i < dates.size(); i++) {
            llistaDies += dates.get(i) + " ";
        }
        builder.setMessage(String.format("¿Has seleccionat els dies: %s estas d'acord?", llistaDies));

        // Si acceptem passem a la següent pestanya
        builder.setPositiveButton("Acceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ferReserva();
            }
        });

        // Si rebutgem, anula les dates reservades i tenim que escollir de nou les dates
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                llistaDeDies.clear();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /*
     * Sino ens dona un missatge de Toast, anirà a aquest métode
     * I es la que ens torna un array amb les dates
     * @param Date startDate
     * @param Date endDate
     * @return un Array amb totes les dates
     * */
    public List<String> DatesEntreDies(Date startDate, Date endDate) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(endDate);

        while (calendar.before(endCalendar)) {
            Date result = calendar.getTime();

            Calendar cal = Calendar.getInstance();
            cal.setTime(result);

            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            String actualDia = year + "-" + checkDigit(month + 1) + "-"
                    + checkDigit(day);
            Log.d("diaActual", actualDia);

            llistaDeDies.add(actualDia);
            calendar.add(Calendar.DATE, 1);
        }
        return llistaDeDies;
    }

    /*
     * Compara la diferencia de dies que te entre l'inici de la data escollida
     * i el dia final de la data
     * @param String diaInici
     * @param String diaFinal
     * @params Context - la Activitat (Reserves)
     * @return String - la diferencia de dies
     * */
    public String difDiaEscollit(String diaInici, String diaFinal) {
        try {
            dataInici = dateFormat.parse(diaInici);
            //Escollim el dia seleccionat, ja que anteriorment l'haviem canviat
            dataIniciSelect = new Date(dataInici.getTime() - 86400000L); // 1dia * 24 * 60 * 60 * 1000
            dataFi = dateFormat.parse(diaFinal);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int dies = (int) (1 + (dataFi.getTime() - dataIniciSelect.getTime()) / 86400000);
        // No es pot reservar mes d'una setmana seguida
        calculDiesSeleccionats(dies, dataIniciSelect, dataFi);

        return String.valueOf(dies);
    }

    /*
     * Calculem els dies que em seleccionat
     * @param int dies seleccionats,
     * @param Date data d'inici de la reserva
     * @param Date data de l'ultima reserva
     * @params Context - la Activitat (Reserves)
     */
    public void calculDiesSeleccionats(int dies, Date inici, Date fi) {
        if (dies > 7) {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.text_inf_caledar2, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        } else {
            DatesEntreDies(inici, fi);
        }
    }

    /*
     * Compara la diferencia de dies que te entre l'inici de la data actual
     * i la data escollida
     * @params ArrayList - array dels clics fets al calendari
     * @params Context - la Activitat (Reserves)
     * @return un String amb la diferencia de dies
     */
    public String difDiaActual(ArrayList<String> arrayD) {

        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        String actualDia = year + "-" + (month + 1) + "-" + day;

        try {
            dataInici = dateFormat.parse(actualDia);
            dataFi = dateFormat.parse(arrayD.get(0));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int dies = (int) (1 + (dataFi.getTime() - dataInici.getTime()) / 86400000);

        // No es pot reservar mes d'una setmana seguida
        calculDiesSeleccionats(dies, dataInici, dataFi);
        return String.valueOf(dies);
    }

    /**
     * Omet el diàleg si es mostra
     */
    private void dismissDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}