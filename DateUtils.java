package com.example.hotspots;

import java.text.SimpleDateFormat;
import java.util.Calendar; 
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class DateUtils {

	private static final String FORMAT_YYYYMMDD_HHMM = "yyyy-MM-dd hh:mm";
	private static final String FORMAT_YYYYMMDD = "yyyy-MM-dd";
	
	private static final long MILIS_HOUR = 1000 * 60 * 60;
	private static final long MILIS_DAY = 1000 * 60 * 60 * 24;
	
	
	private static String calculateDifference(Calendar dateInit, Calendar dateEnd) {
		long diff = dateEnd.getTimeInMillis() - dateInit.getTimeInMillis();
		
		//devolvemos la fecha en formato YYYY-MM-dd
		if (diff >= MILIS_DAY) {			
			return formatToString(dateInit, FORMAT_YYYYMMDD);
		}
		
		//devolvemos las horas que han pasado, por ejemplo "4 hours"
		if (diff >= MILIS_HOUR) {			
			int hours = new Long(TimeUnit.MILLISECONDS.toHours(diff)).intValue();
			return hours == 1 ? hours + " hour" : hours + " hours" ;
		}
		
		int minutes = new Long(TimeUnit.MILLISECONDS.toMinutes(diff)).intValue();
		
		//los minutos, por ejemplo 40 minutes
		return minutes  == 1 ? minutes + " minute" : minutes + " minutes" ;
	}
	
	
	public static Calendar parseToDate(String strDate, String format) throws Exception {
		Calendar cal = Calendar.getInstance();
		
		SimpleDateFormat SDF = new SimpleDateFormat(format);
		
		cal.setTime(SDF.parse(strDate));
		
		return cal;
	}
	
	
	public static String formatToString(Calendar cal, String format) {
		SimpleDateFormat SDF = new SimpleDateFormat(format);
		return SDF.format(cal.getTimeInMillis());
	}
	
	
	
	public static String  diffBetweenNowAnd(String strDate)throws Exception {
		
		if (strDate == null) 
			throw new Exception("Param is null"); 
		
		return calculateDifference(parseToDate(strDate, FORMAT_YYYYMMDD_HHMM),  new GregorianCalendar());		
	}
}
