package com.example.hotspots;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;


/**
 * Clase que ens dona un avís per dir que la connexió a internet es dolenta o inexistent.
 * @author Francisco París
 * @version 02.2020
 */
public class Error404Activity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_error404);
  }
}