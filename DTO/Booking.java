package com.example.hotspots.DTO;

import java.io.Serializable;

/**
 * Data Transfer Object (DTO) – Patró de diseny que permet a l'usuari fer una reserva
 *
 * @author Francisco París
 * @version 02.2020
 */
public class Booking  implements Serializable {
    private String bookingDay;
    private String siteCode;


    public String getBookingDate() {
        return bookingDay;
    }

    public void setBookingDate(String bookingDate) {

        this.bookingDay = bookingDate;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {

        this.siteCode = siteCode;
    }

    @Override
    public String toString() {
        return "MyBooks{" +
                "bookingDay='" + bookingDay + '\'' +
                ", siteCode='" + siteCode + '\'' +
                '}';
    }
}
