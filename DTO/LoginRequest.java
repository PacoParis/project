package com.example.hotspots.DTO;


/**
 * Data Transfer Object (DTO) – Patró de diseny que permet a l'usuari iniciar sessió
 *
 * @author Francisco París
 * @version 02.2020
 */
public class LoginRequest {
    private String email;
    private String password;

    public LoginRequest(String email,String password){
        this.email = email;
        this.password = password;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
