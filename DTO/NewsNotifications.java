package com.example.hotspots.DTO;

public class NewsNotifications {
  private int contador;

  public NewsNotifications(int contador) {
    this.contador = contador;
  }

  public int getContador() {
    return contador;
  }

  public void setContador(int contador) {
    this.contador = contador;
  }
}
