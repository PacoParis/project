package com.example.hotspots.DTO;


/**
 * Model de les notificacions
 * @author Francisco París
 * @version 03.2020
 * */
public class Notification {
  private int id;
  private String message;
  private String createdAt;
  private String type;
  private String isNew;

  public Notification(int id, String message,String createdAt, String isNew) {
    this.id = id;
    this.message = message;
    this.createdAt = createdAt;
    this.isNew = isNew;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getIsNew() {
    return isNew;
  }

  public void setIsNew(String isNew) {
    this.isNew = isNew;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}

