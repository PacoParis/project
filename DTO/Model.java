package com.example.hotspots.DTO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Model de vista de les notificacions
 * @author Francisco París
 * @version 03.2020
 * */
public class Model implements Parcelable {

  private int id;
  public int image;
  public String notification;
  public String title;
  public String description;
  public String isNew;

  public Model() {
  }

  public Model(int image, String notification, String title, String description, String isNew) {
    this.image = image;
    this.notification = notification;
    this.title = title;
    this.description = description;
    this.isNew = isNew;
  }

  public Model(int id, int image, String notification, String title, String description, String isNew) {
    this.id = id;
    this.image = image;
    this.notification = notification;
    this.title = title;
    this.description = description;
    this.isNew = isNew;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNotification() {
    return notification;
  }

  public void setNotification(String notification) {
    this.notification = notification;
  }

  protected Model(Parcel in) {
    image = in.readInt();
    notification = in.readString();
    title = in.readString();
    description = in.readString();
    isNew = in.readString();
  }

  public static final Creator<Model> CREATOR = new Creator<Model>() {
    @Override
    public Model createFromParcel(Parcel in) {
      return new Model(in);
    }

    @Override
    public Model[] newArray(int size) {
      return new Model[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(image);
    dest.writeString(notification);
    dest.writeString(title);
    dest.writeString(description);
    dest.writeString(isNew);
  }
}