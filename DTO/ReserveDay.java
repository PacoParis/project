package com.example.hotspots.DTO;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Data Transfer Object (DTO) – Patró de diseny que permet a l'usuari
 * fer una reserva d'un dia o de varios dies
 *
 * @author Francisco París
 * @version 02.2020
 */

public class ReserveDay {
  private ArrayList allDays;

  public ReserveDay(ArrayList allDays) {
    this.allDays = allDays;
  }

  public ArrayList getAllDays() {
    return allDays;
  }

  public void setAllDays(ArrayList allDays) {
    this.allDays = allDays;
  }
}
