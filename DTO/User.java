package com.example.hotspots.DTO;


/**
 * Data Transfer Object (DTO) – Patró de diseny que permet veure les dades de l'usuari
 *
 * @author Francisco París
 * @version 02.2020
 */

public class User {

  private int id;
  private String email;
  private String password;
  private String name;
  private String lastName;
  private int age;
  private boolean isAdmin;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public boolean isAdmin() {
    return isAdmin;
  }

  public void setAdmin(boolean admin) {
    isAdmin = admin;
  }
}
