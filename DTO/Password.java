package com.example.hotspots.DTO;


/**
 * Data Transfer Object (DTO) – Patró de diseny que permet a l'usuari fer un canvi de reserva
 *
 * @author Francisco París
 * @version 02.2020
 */
public class Password {
  private String password;
  private String oldPassword;
  private int id;

  public Password(String password, String oldPassword) {
    this.password = password;
    this.oldPassword = oldPassword;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getOldPassword() {
    return oldPassword;
  }

  public void setOldPassword(String oldPassword) {
    this.oldPassword = oldPassword;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
