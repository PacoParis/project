package com.example.hotspots.DTO;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class Utils {
  public static Object buildObjectFromResponse(String response, Class cls) {
    if (cls == String.class) {
      return response;
    } else {
      try {
        return new Gson().fromJson(response, cls);
      } catch (JsonSyntaxException jse) {
        return null;
      }
    }
  }
}
