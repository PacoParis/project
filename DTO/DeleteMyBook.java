package com.example.hotspots.DTO;


/**
 * Data Transfer Object (DTO) – Patró de diseny que permet a l'usuari eliminar una reserva
 *
 * @author Francisco París
 * @version 02.2020
 */
public class DeleteMyBook {

    private String bookingDay;

    public DeleteMyBook(String bookingDay) {
        this.bookingDay = bookingDay;
    }

    public String getBookingDay() {
        return bookingDay;
    }

    public void setBookingDay(String bookingDay) {
        this.bookingDay = bookingDay;
    }
}
