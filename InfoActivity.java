package com.example.hotspots;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

public class InfoActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    PDFView pdfView = findViewById(R.id.pdfView);
    pdfView.fromAsset("document_info.pdf")
            .scrollHandle(new DefaultScrollHandle(this))
            .load();
  }
}