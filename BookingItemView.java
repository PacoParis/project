package com.example.hotspots;

import com.example.hotspots.DTO.Booking;

/**
 * Vistes de cada reserva
 * @author Francisco París
 * @version 02.2020
 */
public class BookingItemView {

    private int mImageResourse;

    //los datos de la reserva actual
    private Booking currentReserva;

    /**
     * Constructor que recibe la reserva relacionada a este item de la view
     * @param reserva
     */
    public BookingItemView(Booking reserva, int imageResourse){
        currentReserva = reserva;
        mImageResourse = imageResourse;
    }

    public int getmImageResourse() {
        return mImageResourse;
    }

    public String getSiteCodeToShow() {
        return "Seient: " + currentReserva.getSiteCode();
    }

    public String getBookingDayToShow() {
        return "Dia: " + currentReserva.getBookingDate();
    }

    public Booking getCurrentReserva() {
        return currentReserva;
    }
}
