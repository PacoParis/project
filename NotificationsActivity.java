package com.example.hotspots;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hotspots.DTO.Booking;
import com.example.hotspots.DTO.Model;
import com.example.hotspots.DTO.Notification;
import com.example.hotspots.DTO.Utils;
import com.example.hotspots.network.HotspotApi;
import com.example.hotspots.network.NetworkRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.file.FileVisitOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Activitat de les notificacions rebudes durant la teva descarrega de l'app
 * @author Francisco París
 * @version 03.2020
 * */
public class NotificationsActivity extends AppCompatActivity {
  private Adapter adapter;
  private RecyclerView recyclerView;
  private List<Model> modelList;
  private String notificationTime;
  private AuthHelper mAuthHelper;
  private ProgressDialog mProgressDialog;

  // visualització de text de la insígnia de notificacions
  private TextView badgeCounter;
  private MenuItem menuItem;
  private int notificacions = 0;


  /*
   * Resposta de trucada per a la llista de notificacions
   *  Depenent del missatge de sortida, enviarem un Toast per coneixer si s'ha
   *  fet correctament la reserva
   * */
  private final NetworkRequest.Callback<String> notificationCallBack = new NetworkRequest.Callback<String>() {
    @Override
    public void onResponse(@NonNull String response) {
      Log.d(Constants.LOG_TAG, "Logout server response 1:" + response);
      getMyNotifications(response);
      dismissDialog();
    }

    @Override
    public void onError(String error) {
      Log.d("HOTSPOT_DEBUG2", "Logout server response error:" + error);
      dismissDialog();
    }

    @Override
    public Class<String> type() {
      return String.class;
    }
  };



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_notifications);

    // Passem el número de notificacions pendents
    Intent mIntent = getIntent();
    notificacions = mIntent.getIntExtra("notificacionsPendents", 0);

    mAuthHelper = AuthHelper.getInstance(this);
    mostraNotificacions();

  }

  /*
   * Possem una icono o insignia amb forma d'una campana
   * al menú per veure les notificacions pendents
   * */
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);

    menuItem = (MenuItem)menu.findItem(R.id.nav_notification);

    // comproveu si hi ha alguna notificació pendent
    if (notificacions == 0) {
      // si no hi ha cap notificació pendent, traieu la insígnia
      menuItem.setActionView(null);
    } else {

      // si es notifica que estableix el disseny de la icona de la insígnia
      menuItem.setActionView(R.layout.notification_badge);
      // obtenir la vista des de l'element de navegació
      View view = menuItem.getActionView();
      // obtenir la vista de text de la vista d'acció per a l'element de navegació
      badgeCounter = view.findViewById(R.id.badge_counter);
      // definiu el valor de les notificacions pendents
      badgeCounter.setText(String.valueOf(notificacions));

    }

    menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
      @Override
      public boolean onMenuItemClick(MenuItem item) {
        Intent intent = new Intent(getApplicationContext(), NotificationsActivity.class);
        startActivity(intent);
        return false;
      }
    });

    return super.onCreateOptionsMenu(menu);
  }


  /*
   * Mostra la llista de notificacions i ens dona un dialeg informatiu
   * explicant el funcionament de la activitat
   */
  public void mostraNotificacions() {

    final ProgressDialog progress = new ProgressDialog(this, R.style.MyAlertDialogStyle);
    progress.setTitle(R.string.msn_dialog_notifications);
    progress.setMessage("Desplaça sobre les notificacions per veure tots els missatges");
    progress.show();
    Runnable progressRunnable = new Runnable() {

      @Override
      public void run() {
        progress.cancel();
      }
    };

    Handler pdCanceller = new Handler();
    pdCanceller.postDelayed(progressRunnable, 800);

    HotspotApi request = new HotspotApi();

    request.showMyNotifications(notificationCallBack, mAuthHelper.getIdToken());
  }


  /**
   * Obté totes les meves notificacions from JSON response
   * @param response - String
   */
  public void getMyNotifications(String response) {

    modelList = new ArrayList<>();

    try {
      JSONArray jsonArray = new JSONArray(response);

      Notification currentNotification;

      for (int i = 0; i < jsonArray.length(); i++){

        currentNotification = (Notification) Utils.buildObjectFromResponse(jsonArray.get(i).toString(),Notification.class);

        // Controlem quan has rebut la teva notificació
        try {
          notificationTime = ( DateUtils.diffBetweenNowAnd(currentNotification.getCreatedAt()));

          // Per fer proves amb la notificació... si es recent provem canviant l'horari de la
          // notificació manualment
          // notificationTime = ( DateUtils.diffBetweenNowAnd("2020-11-24 10:22"));

        } catch (Exception e) {
          e.printStackTrace();
        }

        //Depenent del tipus d'error que ens doni, sortirà una imatge o un altra
        if((currentNotification.getType()).contains("WARNING")){

          modelList.add(new Model(currentNotification.getId(), R.drawable.danger, notificationTime, currentNotification.getType(),
                  currentNotification.getMessage(), currentNotification.getIsNew() ));
        }

        if((currentNotification.getType()).contains("NOTIFICATION")){

          modelList.add(new Model(currentNotification.getId(), R.drawable.remember, notificationTime, currentNotification.getType(),
                  currentNotification.getMessage(), currentNotification.getIsNew()));
        }

        if((currentNotification.getType()).contains("ERROR") ||
          (currentNotification.getMessage().contains("borrado"))){

          modelList.add(new Model(currentNotification.getId(), R.drawable.cancel, notificationTime, currentNotification.getType(),
                  currentNotification.getMessage(), currentNotification.getIsNew() ));
        }

      }

      // Li passem el RecyclerView
      recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
      recyclerView.setHasFixedSize(true);
      recyclerView.setLayoutManager(new LinearLayoutManager(this));

      adapter = new Adapter(modelList, this, R.id.frameLayout);
      recyclerView.setAdapter(adapter);


    } catch (JSONException e) {
      e.printStackTrace();
    }

  }

  /**
   * Omet el diàleg si es mostra
   */
  private void dismissDialog() {

    if (mProgressDialog != null && mProgressDialog.isShowing()) {
      mProgressDialog.dismiss();
    }
  }

}