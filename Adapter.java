package com.example.hotspots;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotspots.DTO.Model;
import com.example.hotspots.DTO.Notification;
import com.example.hotspots.DTO.Utils;
import com.example.hotspots.network.HotspotApi;
import com.example.hotspots.network.NetworkRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<ViewHolder> {

  private List<Model> listData;
  private Context context;
  private int layout;
  private AuthHelper mAuthHelper;

  public Adapter(List<Model> listData, Context context, int layout) {
    this.listData = listData;
    this.context = context;
    this.layout = layout;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View itemView = inflater.inflate(R.layout.item_view, parent, false);
    mAuthHelper = AuthHelper.getInstance(context);
    return new ViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull final ViewHolder holder,final int position) {
    holder.titleText.setText(listData.get(position).title);
    holder.descriptionText.setText(listData.get(position).description);
    holder.notification.setText(listData.get(position).notification);

    // Si la notificacio es false, significa que ja s'ha vist anteriorment
    // per tant ocultarem el text i la imatge del punt vermell
    // (indicador de nova notificació)
    if((listData.get(position).isNew).contains("false")){
      holder.novaNot.setText("");
      holder.imgNot.setVisibility(View.INVISIBLE);
    }

    Picasso.get().load(listData.get(position).image).into(holder.iconImage);

    holder.cardId.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        //Si la notificació pulsada isNew, vol dir que no s'ha vist encara
        // per tant, al pulsar canviarem el seu estat a Invisible
        if((listData.get(position).isNew).contains("true")){
          //Ho faig manualment ara pero possem el id de la notificació
          //holder.novaNot.setText("");
          //holder.imgNot.setVisibility(View.INVISIBLE);

          // Ara elimino la notificació amb el seu ID real
          markNotificationAsRead(String.valueOf(listData.get(position).getId()));

        }

        // Veiem els detalls de la notificació
        context.startActivity(new Intent(context, ViewDetails.class)
                .putExtra("id", listData.get(position)));

      }
    });

    holder.cardId.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        return false;
      }
    });

  }

  @Override
  public int getItemCount() {
    return listData.size();
  }


  /**
   * Trucades a la base de dades per marcar la notificació com a llegida
   *
   */
  private void markNotificationAsRead(String id) {
    HotspotApi request = new HotspotApi();
    request.markNotification(markNotificationCallback, mAuthHelper.getIdToken(), id);
  }

  /**
   * Callback for mark notification as read
   *
   */
  private NetworkRequest.Callback<String> markNotificationCallback = new NetworkRequest.Callback<String>() {
    @Override
    public void onResponse(@NonNull String response) {
      Log.d("HOTSPOT_DEBUG", "Mark as read server response:" +  response);
      //Toast.makeText(ActivityDetailNotification.this, response, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String error) {
      Log.d("HOTSPOT_DEBUG", "Mark as read server response error:" +  error);
      //Toast.makeText(ActivityDetailNotification.this, "Error en la modificació", Toast.LENGTH_SHORT).show();
    }

    @Override
    public Class<String> type() {
      return String.class;
    }
  };


}
