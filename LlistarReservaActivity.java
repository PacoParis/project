package com.example.hotspots;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.example.hotspots.DTO.Booking;
import com.example.hotspots.DTO.Utils;
import com.example.hotspots.network.HotspotApi;
import com.example.hotspots.network.NetworkRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Clase que permet llistar a l'usuari totes les reserves fetes per nosaltres.
 * @author Francisco París
 * @version 02.2020
 */
public class LlistarReservaActivity extends AppCompatActivity implements android.view.View.OnClickListener {
    private final ArrayList<BookingItemView> viewRes = new ArrayList<>();
    private final ArrayList<Booking> misReservas = new ArrayList<>();
    private String TAG = "MyTag";
    private boolean opened;
    private LinearLayout view;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView delete, msnInfNoReserves;
    private Button btn_map, btn_menu;
    private ProgressDialog mProgressDialog;
    private AuthHelper mAuthHelper;
    private boolean oldBook = false;


    /*
     * Resposta de trucada per a la llista de reserva de dies en l'oficina
     *  Depenent del missatge de sortida, enviarem un Toast per coneixer si s'ha
     *  fet correctament la reserva
     * */
    private final NetworkRequest.Callback<String> myBooksCallBack = new NetworkRequest.Callback<String>() {
        @Override
        public void onResponse(@NonNull String response) {
            Log.d(Constants.LOG_TAG, "Logout server response 1:" + response);
            getMyBooks(response);
            dismissDialog();
        }

        @Override
        public void onError(String error) {
            Log.d("HOTSPOT_DEBUG2", "Logout server response error:" + error);
            dismissDialog();
        }

        @Override
        public Class<String> type() {
            return String.class;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_llistar_reserva);

        mAuthHelper = AuthHelper.getInstance(this);
        //mProgressDialog = new ProgressDialog(this,R.style.MyAlertDialogStyle);

        mostrarReserva(); // Mostrem la llista de reserves

        mRecyclerView = findViewById(R.id.recview);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);

        msnInfNoReserves = findViewById(R.id.noTensReserves);
        btn_map = findViewById(R.id.btn_mapa);
        btn_menu = findViewById(R.id.btn_menu);
        btn_map.setOnClickListener(this);
        btn_menu.setOnClickListener(this);
        view = findViewById(R.id.view);
        view.setVisibility(android.view.View.INVISIBLE);

        /*delete = findViewById(R.id.deleteItem);
        Intent intent = getIntent();
        posicio = intent.getStringExtra("elementMarcat");
        delete.setText(posicio);*/

    }

    /*
     * Mostra la llista de reserves i ens dona un dialeg informatiu
     * explicant el funcionament de la activitat
     */
    public void mostrarReserva() {

        final ProgressDialog progress = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        progress.setTitle(R.string.msn_dialog_tittle_list_books);
        progress.setMessage("Desplaça sobre els tickets per poder veure totes" +
                " les reserves fetes.\nSi vols cancelar una reserva, fes click a sobre.");
        progress.show();
        Runnable progressRunnable = new Runnable() {

            @Override
            public void run() {
                progress.cancel();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 4000);

        HotspotApi request = new HotspotApi();
        //Log.d(TAG, mAuthHelper.getIdToken());
        request.showMyReserves(myBooksCallBack, mAuthHelper.getIdToken());
    }

    @Override
    public void onClick(android.view.View v) {
        switch (v.getId()) {

            case R.id.btn_mapa:
                translationPhoto();
                return;

            case R.id.btn_menu:
                Intent i = new Intent(getApplicationContext(), QuotesActivity.class);
                startActivity(i);
                return;

        }
    }

    /*
     * Mostra/oculta el plànol de l'oficina
     * */
    private void translationPhoto() {
        if (!opened) {
            view.setVisibility(android.view.View.VISIBLE);
            TranslateAnimation animate = new TranslateAnimation(
                    0,                 // fromXDelta
                    0,                 // toXDelta
                    view.getHeight(),  // fromYDelta
                    0);                // toYDelta
            animate.setDuration(800);
            animate.setFillAfter(true);
            view.startAnimation(animate);
            btn_map.setText("OCULTAR PLÀNOL");
        } else {
            TranslateAnimation animate = new TranslateAnimation(
                    0,                 // fromXDelta
                    0,                 // toXDelta
                    0,                 // fromYDelta
                    view.getHeight() + 500); // toYDelta
            animate.setDuration(800);
            animate.setFillAfter(true);
            view.startAnimation(animate);
            btn_map.setText("PLÀNOL OFICINA");
        }
        opened = !opened;
    }

    /**
     * Obté totes les meves reserves from JSON response
     * @param response - String
     */
    public void getMyBooks(String response) {

        try {
            //El missatge dient que no existeixen reserves desapareix
            msnInfNoReserves.setVisibility(View.INVISIBLE);

            JSONObject object = new JSONObject(response);
            JSONArray jsonArray = object.getJSONArray("bookings");

            Booking currentReserva;
            BookingItemView currentBookingView;

            for (int i = 0; i < jsonArray.length(); i++) {

                currentReserva = (Booking) Utils.buildObjectFromResponse(jsonArray.get(i).toString(), Booking.class);

                Log.d(Constants.LOG_TAG, "Datos reserva actual: " + currentReserva.toString());

                if(currentDay(currentReserva.getBookingDate()) == true){
                    currentBookingView = new BookingItemView(currentReserva, R.drawable.waitingroom);
                }
                else{
                    currentBookingView = new BookingItemView(currentReserva, R.drawable.seat_24);
                }

                misReservas.add(currentReserva);

                //Afegim a la vista del adapter
                viewRes.add(currentBookingView);
            }

            //Aquí li pasem el adapter... Una vegada recollida tota la llista de reserves
            mAdapter = new MyAdapter(viewRes, getApplicationContext(), misReservas);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mAdapter);

            Log.d("total de reserves:", String.valueOf(misReservas.size()));

            dismissDialog();

        } catch (JSONException e) {
            Log.d("HOTSPOT_DEBUG", e.toString());

            if(e.getMessage().contains("Value null at bookings")){
                Log.d("Tens reserves?", " No, no hi han reserves");
                msnInfNoReserves.setVisibility(View.VISIBLE);
            }


        }
    }

    /*
     * Compara el dia actual amb cada una de les reserves fetes
     * @param  bookingDay - String
     * @return oldBook - Si la reserva es antiga, oldBook = true
     */
    private boolean currentDay(String bookingDay) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date dateBookingDay = null;

        try {
            dateBookingDay = format.parse(bookingDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long prevDay = System.currentTimeMillis() - 1000*60*60*24;
        Date prev = new Date(prevDay);

        //compara cada dia reservat amb la data del dia anterior al d'avuí
        if(prev.before(dateBookingDay)){
            Log.d(TAG, "The date is pass day");
            oldBook = true;

        } else {
            Log.d(TAG, "The date is a future day");
            oldBook = false;
        }
        return oldBook;
    }

    /**
     * Omet el diàleg si es mostra
     */
    private void dismissDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
