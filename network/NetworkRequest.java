package com.example.hotspots.network;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.hotspots.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.Map;

import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Classe de sol·licitud de xarxa per resumir els detalls de la implementació de les sol·licituds
 *
 * @author Francisco París
 * @version 01.2020
 */

public class NetworkRequest extends Activity {

    private Callback mCallback;
    private OkHttpClient mClient;

    public NetworkRequest() {
        mClient = new OkHttpClient();
    }

    /**
     * Estableix la devolució de trucada per a la sol·licitud de xarxa
     *
     * @param callback
     */
    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    /**
     * Executa la sol·licitud de publicació
     *
     * @param url
     * @param objectParam
     * @param callback
     * @param token
     */
    protected void doPostRequest(@NonNull String url, Object objectParam, @Nullable final Callback callback, @NonNull String token) {
        HttpUrl httpUrl = HttpUrl.parse(url);

        String str = new Gson().toJson(objectParam);

        Log.d(Constants.LOG_TAG, String.format("Making request to::(%s) with json(%s)", url, str));

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, str);

        Request.Builder requestBuilder = new Request.Builder()
                .url(httpUrl)
                .post(body);

        if (token != null) {
            requestBuilder.addHeader("Authorization", "Bearer " + token);
        }

        doRequest(requestBuilder.build(), callback);
    }

    protected void doGetRequest(@NonNull String url, @NonNull String token, @Nullable Callback callback) {

        HttpUrl httpUrl = HttpUrl.parse(url);

        HttpUrl.Builder urlBuilder = httpUrl.newBuilder();

        Request.Builder requestBuilder = new Request.Builder()
                .url(urlBuilder.build())
                .get();

        if (token != null) {
            requestBuilder.addHeader("Authorization", "Bearer " + token);
        }

        doRequest(requestBuilder.build(), callback);
    }

    protected void doGetRequestWithToken(@NonNull String url,@NonNull Map<String, String> params,
                                         @Nullable String token,@Nullable Callback callback) {
        HttpUrl httpUrl = HttpUrl.parse(url);

        HttpUrl.Builder urlBuilder = httpUrl.newBuilder();
        for (String key : params.keySet()) {
            urlBuilder.addQueryParameter(key, params.get(key));
        }

        Request.Builder requestBuilder = new Request.Builder()
                .url(urlBuilder.build())
                .get();

        if (token != null) {
            requestBuilder.addHeader("Authorization", "Bearer " + token);
        }

        doRequest(requestBuilder.build(), callback);
    }

    /**
     * Fa la sol·licitud i dispara la devolució de trucada en el moment oportú
     *
     * @param request
     * @param callback
     */
    private void doRequest(@NonNull Request request, final Callback callback) {
        mClient.newCall(request).enqueue(new okhttp3.Callback() {
            Handler mainHandler = new Handler(Looper.getMainLooper());

            @Override
            public void onFailure(Call call, final IOException e) {
                if (callback != null) {
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(e.toString());
                        }
                    });
                }
            }

            @Override
            public void onResponse(final Call call, final Response response) {
                if (callback != null) {
                    try {
                        final String stringResponse = response.body().string();
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Object res = buildObjectFromResponse(stringResponse,
                                        callback.type());
                                if (res != null) {
                                    callback.onResponse(res);
                                } else {
                                    callback.onError(stringResponse);
                                }
                            }
                        });
                    } catch (final IOException ioe) {
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onError(ioe.toString());
                            }
                        });
                    }
                }
            }
        });
    }

    /**
     * Construcció de la resposta
     */
    private Object buildObjectFromResponse(String response, Class cls) {
        if (cls == String.class) {
            return response;
        } else {
            try {
                return new Gson().fromJson(response, cls);
            } catch (JsonSyntaxException jse) {
                return null;
            }
        }
    }

    /**
     * Interfície de devolució de trucada per a la resposta i error de la xarxa
     *
     * @param <T>
     */
    public interface Callback<T> {
        void onResponse(@NonNull T response);

        void onError(String error);

        Class<T> type();
    }

    /**
     * Interfície ApiResponse
     */
    public interface ApiResponse {
        String string();
    }

}