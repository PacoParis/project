package com.example.hotspots.network;

import com.google.gson.annotations.SerializedName;

/** Classe de les dades de petició del Token
 * @author Francisco París
 * @version 01.2020 */

public class Token implements NetworkRequest.ApiResponse {
    @SerializedName("access_token")
    private String idToken;

    public String getIdToken() {
        return idToken;
    }

    @Override
    public String string() {
        return idToken;
    }
}
