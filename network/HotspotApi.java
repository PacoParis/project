package com.example.hotspots.network;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.hotspots.Constants;
import com.example.hotspots.DTO.DeleteMyBook;
import com.example.hotspots.DTO.Password;
import com.example.hotspots.DTO.ReserveDay;
import com.example.hotspots.DTO.User;
import com.example.hotspots.DTO.LoginRequest;
import com.example.hotspots.DTO.Utils;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Classe que permet fer el login, logout, canvi de contrasenya i actualització de l'usuari
 *
 * @author Francisco París
 * @version 01.2020
 */
public class HotspotApi extends NetworkRequest {
    private OkHttpClient mClient;

    /**
     * Login de la app
     *
     * @param username - username
     * @param password - password
     * @param callback - callback
     */
    public void doLogin(@NonNull String username,
                        @NonNull String password,
                        Callback callback) {
        setCallback(callback);

        LoginRequest logReq = new LoginRequest(username, password);

        String url = Constants.BASE_URL + Constants.LOGIN_ENDPOINT;

        Log.d(Constants.LOG_TAG, "Making request to1:" + url);

        doPostRequest(url, logReq, callback, null);
    }

    /**
     * Logout de la app
     *
     * @param callback - callback
     * @param token    - String
     */
    public void doLogout(@NonNull Callback callback, @NonNull String token) {

        String url = Constants.BASE_URL + Constants.LOGOUT_ENDPOINT;

        Log.d(Constants.LOG_TAG, "Making request to2:" + url);

        doGetRequest(url, token, callback);
    }

    /**
     * Canvia només el teu password de la app
     *
     * @param callback    - callback
     * @param token       - String
     * @param newPassword - String
     * @param oldPassword - String
     */
    public void changePassword(@NonNull Callback callback, @NonNull String token, @NonNull String newPassword, @NonNull String oldPassword) {
        Password password = new Password(newPassword, oldPassword);
        updatePassword(callback, token, password);
    }

    /**
     * Actualitza el teu usuari de la app
     *
     * @param callback - callback
     * @param token    - String
     * @param user     - Object
     */
    private void updateUser(@NonNull Callback callback, @NonNull String token, @NonNull User user) {
        String url = Constants.BASE_URL + Constants.UPDATE_USER_ENDPOINT;
        Log.d(Constants.LOG_TAG, "Making request to:3" + url);
        doPostRequest(url, user, callback, token);
    }

    /**
     * Actualitza el teu password de la app
     *
     * @param callback - callback
     * @param token    - String
     * @param password - Object
     */
    private void updatePassword(@NonNull Callback callback, @NonNull String token, @NonNull Password password) {
        String url = Constants.BASE_URL + Constants.UPDATE_PASSWORD_ENDPOINT;
        Log.d(Constants.LOG_TAG, "Making request to4:" + url);
        doPostRequest(url, password, callback, token);
    }

    /**
     * Reserva dia en l'oficina
     *
     * @param token    - username
     * @param dates    - password
     * @param callback - callback
     */
    public void doReserve(@NonNull String token, @NonNull ArrayList dates, Callback callback) {
        ReserveDay reserveDay = new ReserveDay(dates);

        String url = Constants.BASE_URL + Constants.BOOK_SITE;

        Log.d(Constants.LOG_TAG, "Making request to::" + url);

        doPostRequest(url, reserveDay, callback, token);

    }

    /**
     * Mostrar les teves reserves efectuades en l'oficina
     *
     * @param callback - callback
     * @param token    - username
     */
    public void showMyReserves(Callback callback, @NonNull String token) {

        String url = Constants.BASE_URL + Constants.MY_BOOKS;

        Log.d(Constants.LOG_TAG, "Making request to::" + url);

        doGetRequest(url, token, callback);

    }

    /**
     * Elimina la meva reserva seleccionada
     *
     * @param callback   - callback
     * @param token      - username
     * @param bookingDay - String
     */
    public void deleteMyReserve(@NonNull Callback callback, @NonNull String token, @NonNull String bookingDay) {

        String url = Constants.BASE_URL + Constants.DELETE_MY_BOOK;

        DeleteMyBook deleteRequest = new DeleteMyBook(bookingDay);

        Log.d(Constants.LOG_TAG, "Making request to::" + url);

        doPostRequest(url, deleteRequest, callback, token);
    }

    /**
     * Mostrar les notificacions rebudes
     *
     * @param callback - callback
     * @param token    - username
     */
    public void showMyNotifications(Callback callback, @NonNull String token) {

        String url = Constants.BASE_URL + Constants.MY_NOTIFICATIONS;

        Log.d(Constants.LOG_TAG, "Making request to::" + url);

        doGetRequest(url, token, callback);

    }

    /**
     * Marca la notificació com a identificadora llegida
     *
     * @param callback
     * @param token
     * @param id - des de la notificació que vulgueu marcar com a llegida
     * **/
    public void markNotification(@NonNull Callback callback, @NonNull String token, @NonNull String id){
        String url = Constants.BASE_URL + Constants.MARK_NOTIFICATION_ENDPOINT;
        Log.d("HOTSPOT_DEBUG", "Making request to:" + url + " id notification "+id);

        Map notId = new HashMap();
        notId.put("notificationID",id);

        doGetRequestWithToken(url, notId, token, callback);
    }

}
