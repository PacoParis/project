package com.example.hotspots;

/**
 * Clase que te totes les constants
 * @author Francisco París
 * @version 02.2020
 */
public class Constants {


  public static final String BASE_URL = "https://556cca31ba83.ngrok.io";

  public static final String LOGIN_ENDPOINT = "/login";
  public static final String LOGOUT_ENDPOINT = "/logout";
  public static final String UPDATE_USER_ENDPOINT = "/user/update";
  public static final String UPDATE_PASSWORD_ENDPOINT = "/user/updatePassword";
  public static final String BOOK_SITE = "/booking/bookSite";
  public static final String MY_BOOKS = "/booking/allMy";
  public static final String DELETE_MY_BOOK = "/booking/delete";
  public static final String MY_NOTIFICATIONS = "/notification/allMy";
  public static final String MARK_NOTIFICATION_ENDPOINT = "/notification/markAsRead";

  public static final String LOG_TAG = "HOTSPOT_DEBUG";

  public static final String CONTACTE_POSITIU =
          "\n ° Han de fer quarentena durant els 10 dies següents al contacte amb un cas diagnosticat, " +
          "restringint al màxim el contacte amb convivents i utilitzant mascareta quirúrgica en les zones comuns.\n" +
          "\n ° Han de fer el seguiment i vigilància dels símptomes a través de l’aplicació STOP COVID-19.\n" +
          "\n ° Si presenta símptomes durant aquest període ha de posar-se en contacte amb el seu equip " +
          "d’Atenció Primària, excepte el cap de setmana que ha de trucar al 061.\n" +
          "\n ° Si transcorreguts 10 dies des de l’última vegada que va tenir contacte amb el cas " +
          "diagnosticat, no ha tingut cap símptoma, pot retornar a la seva rutina habitual. No  obstant" +
          " això,  es  recomana  prudència  especial  entre  els  dies  10  i  14,    sempre  amb mascareta" +
          " quirúrgica,  minimitzant els contactes  estrets  i,  si  apareixen  símptomes auto-confinament  " +
          "immediat.\n";

  public static final String CANCELACIO_RESERVA =
          "\n ° L'amfitrió ha cancel·lat la reserva, rebràs un reemborsament total de forma automàtica. " +
          "Et arribarà un correu electrònic de confirmació, amb un enllaç per comprovar el seu estat. " +
          "\n ° Des de fa unes setmanes, les alarmes han saltat per culpa de l'Covid-19." +
          "\n ° Degut a el problema de l'aforament, reservem el dret a tenir unes mesures per poder " +
          "mantenir la suficient distància de seguretat. Degut a aquest imprevist, ens hem vist obligats" +
          " a anul·lar alguna de les reserves, incloent-hi la seva. Disculpi les molèsties.\n";

  public static final String NOTIFICACIO_ERROR =
          "\n ° Download the perfect remember pictures. Find over 100+ of the best free " +
          "remember images. Free for commercial use ✓ No attribution requiredDownload the perfect " +
          "remember pictures. \n ° Find over 100+ of the best free remember images. Free for commercial " +
          "use ✓ No attribution required Download the perfect remember pictures. Find over 100+ of " +
          "the best free remember images. \n ° Free for commercial use ✓ No attribution required. \n";

  public static final String RECORDATORI =
          "\n ° Aquest es un missatge recordatori. No tens que constestar ni fer res. " +
          "\n ° Quan arribis recorda rentar-te les mans i anar al teu seient assignat." +
          "\n ° Desitgem que gaudeixis de la teva estancia i segueixis confiant amb " +
          "nosaltres. Gracies \n";
}
