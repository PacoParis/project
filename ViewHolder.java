package com.example.hotspots;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotspots.DTO.Model;
import com.example.hotspots.DTO.Notification;
import com.example.hotspots.DTO.Utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Vista detallada del Recycerview
 * @author Francisco París
 * @version 03.2020
 * */
public class ViewHolder extends RecyclerView.ViewHolder {

  public CardView cardId;
  public ImageView iconImage, imgNot;
  public TextView titleText, descriptionText, notification, novaNot;
  private LinearLayout novaNotificacio;


  public ViewHolder(@NonNull View itemView) {
    super(itemView);

    iconImage = (ImageView) itemView.findViewById(R.id.iconImage);

    cardId = (CardView) itemView.findViewById(R.id.cardId);

    titleText = (TextView) itemView.findViewById(R.id.titleText);

    descriptionText = (TextView) itemView.findViewById(R.id.descriptionText);

    notification = (TextView) itemView.findViewById(R.id.notificationText);

    novaNot=(TextView) itemView.findViewById(R.id.textNovaNot);

    imgNot=(ImageView) itemView.findViewById(R.id.imgNot);

  }
}

