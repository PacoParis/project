package com.example.hotspots;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hotspots.network.HotspotApi;
import com.example.hotspots.network.NetworkRequest;
import com.example.hotspots.network.Token;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.ContentValues.TAG;

/**
 * Classe d'inici de sessió
 * @author Francisco París
 * @version 01.2020
 */

public class LoginActivity extends AppCompatActivity {

    private TextView mTitleAction;
    private EditText mEditEmail;
    private EditText mEditPassword;
    private Button mButtonAction;

    private ProgressDialog mProgressDialog;
    private AuthHelper mAuthHelper;

    //Marca per mostrar si es mostra el camp de registre
    private boolean mIsSignUpShowing;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuthHelper = AuthHelper.getInstance(this);
        mProgressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);

        mTitleAction = (TextView) findViewById(R.id.text_title);
        mEditEmail = (EditText) findViewById(R.id.edit_email);
        mEditPassword = (EditText) findViewById(R.id.edit_password);
        mButtonAction = (Button) findViewById(R.id.button_action);

        // Truca per telefon en cas de dupte si prems al FAB
        fab=(FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNo = "933476100";  // teléfon de la IOC
                String dial = "tel:" + phoneNo;
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
            }
        });

        setupView(mIsSignUpShowing);

        if (mAuthHelper.isLoggedIn()) {
            startActivity(QuotesActivity.getCallingIntent(this));
        }
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }


    /**
     * Configura la vista en funció de si es mostra o no la pantalla de registre
     * @param isSignUpShowing - indicador que indica si es mostra el formulari d’inscripció
     */
    private void setupView(boolean isSignUpShowing) {
        mIsSignUpShowing = isSignUpShowing;
        mTitleAction.setText(isSignUpShowing ? R.string.text_sign_up : R.string.text_login);
        mButtonAction.setText(isSignUpShowing ? R.string.text_sign_up : R.string.text_login);
        mButtonAction.setOnClickListener(isSignUpShowing ? doSignUpClickListener : doLoginClickListener);
    }

    /**
     * Comprova si el String introduit es un correu real
     * @return torna true o false
     */
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Inicieu la sessió a l’usuari i aneu a la pantalla del perfil si és correcte
     *  En cas de no ser correcte enviar un Toast depenent de si el format de correu es
     *  erroni, algun camp es vuit o l'usuari no està registat
     */
    private void doLogin() {
        String username = getUsernameText();
        String password = getPasswordText();

        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.toast_no_empty_field, Toast.LENGTH_SHORT).show();
            return;

        }else {
            if(isEmailValid(username)){
                mProgressDialog.setMessage(getString(R.string.progress_login));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();

                HotspotApi request = new HotspotApi();
                request.doLogin(username, password, mLoginCallback);
                Log.d("Error", mLoginCallback.toString()+ " " + password);
            }else{
                Toast.makeText(getApplicationContext(),R.string.error_email_format, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Registreu l'usuari i aneu a la pantalla del perfil
     * */
    private void doSignUp() {
        String username = getUsernameText();
        String password = getPasswordText();

        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.toast_no_empty_field, Toast.LENGTH_SHORT).show();
            return;
        }

        mProgressDialog.setMessage(getString(R.string.progress_signup));
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();

    }

    public String getUsernameText() {
        return mEditEmail.getText().toString().trim();
    }

    private String getPasswordText() {
        return mEditPassword.getText().toString().trim();
    }


    /**
     * Deseu els detalls de la sessió i navegueu fins a l'activitat de les cometes
     * @param token - {@link Token} received on login or signup
     */
    private void saveSessionDetails(@NonNull Token token) {
        mAuthHelper.setIdToken(token);

        // iniciar l’activitat del perfil
        startActivity(QuotesActivity.getCallingIntent(this));
    }

    /**
     * Devolució de trucada per iniciar la sessió
     */
    private NetworkRequest.Callback<Token> mLoginCallback = new NetworkRequest.Callback<Token>() {
        @Override
        public void onResponse(@NonNull Token response) {
            dismissDialog();
            // deseu el testimoni i aneu a la pàgina de perfil
            saveSessionDetails(response);
        }

        // L'usuari no està registat
        @Override
        public void onError(String error) {
            dismissDialog();
            Log.d(Constants.LOG_TAG, error);
            if(error.contains("Connection closed by peer")){
                Toast.makeText(getApplicationContext(),"Error amb la conexió", Toast.LENGTH_LONG).show();
                Intent i = new Intent(LoginActivity.this, Error404Activity.class);
                startActivity(i);

            }
            if(error.contains("not found")){
                Toast.makeText(getApplicationContext(),"Error amb la conexió", Toast.LENGTH_LONG).show();
                Intent i = new Intent(LoginActivity.this, Error404Activity.class);
                startActivity(i);

            }
            if(error.contains("Please provide valid login details.")){
                Toast.makeText(getApplicationContext(),R.string.user_not_register, Toast.LENGTH_LONG).show();
            }
            if(error.contains("No address associated with hostname")){
                Toast.makeText(getApplicationContext(),"Error amb la conexió", Toast.LENGTH_LONG).show();
                Intent i = new Intent(LoginActivity.this, Error404Activity.class);
                startActivity(i);

            }
        }
        @Override
        public Class<Token> type() {
            return Token.class;
        }

    };

    /**
     * Devolució de trucada per registrar-se
     */
    private NetworkRequest.Callback<Token> mSignUpCallback = new NetworkRequest.Callback<Token>() {
        @Override
        public void onResponse(@NonNull Token response) {
            dismissDialog();
            // deseu el testimoni i aneu a la pàgina de perfil
            saveSessionDetails(response);
        }

        @Override
        public void onError(String error) {
            dismissDialog();
            Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
        }

        @Override
        public Class<Token> type() {
            return Token.class;
        }
    };

    /**
     * Omet el diàleg si es mostra
     */
    private void dismissDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * Feu clic a l’oient per mostrar el formulari d’inscripció
     */
    private final View.OnClickListener showSignUpFormClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setupView(true);
        }
    };

    /**
     * Feu clic a l'oient per mostrar el formulari d'inici de sessió
     */
    private final View.OnClickListener showLoginFormClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setupView(false);
        }
    };

    /**
     * Feu clic a oient per invocar l'inici de sessió
     */
    private final View.OnClickListener doLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            doLogin();
        }
    };

    /**
     * Feu clic a oient per invocar el registre
     */
    private final View.OnClickListener doSignUpClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            doSignUp();
        }
    };


}
