package com.example.hotspots;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotspots.DTO.Booking;
import com.example.hotspots.network.HotspotApi;
import com.example.hotspots.network.NetworkRequest;

import java.util.ArrayList;

/**
 * Embolcall entre dos objectes. Atrapa les trucades a un objecte i les transforma a un
 * format i una interfície reconeixible per al segon objecte.
 * @author Francisco París
 * @version 02.2020
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ExampleViewHolder> {

    private static ClickListener clickListener;
    private final Context mContext;
    private final Bundle mBundle = new Bundle();
    private final ArrayList<Booking> myBooksArrayList;
    private final AuthHelper mAuthHelper;
    public ArrayList<BookingItemView> mExampleList;
    private Integer pos;


    public MyAdapter(ArrayList<BookingItemView> exampleList,
                     Context context, ArrayList<Booking> myBooksArrayList) {
        mExampleList = exampleList;
        mContext = context;
        this.myBooksArrayList = myBooksArrayList;
        mAuthHelper = AuthHelper.getInstance(mContext.getApplicationContext());

    }


    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view, parent, false);
        ExampleViewHolder evh = new ExampleViewHolder(v);
        return evh;
    }

    /*
     * Llista que mostra gran quantitat de dades de forma ordenada i clara
     */
    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, final int position) {
        final BookingItemView currentItemView = mExampleList.get(position);

        holder.mImageView.setImageResource(currentItemView.getmImageResourse());
        holder.mTextSeient.setText(currentItemView.getSiteCodeToShow());
        holder.mTextReserva.setText(currentItemView.getBookingDayToShow());

        //Avisem d'alguna manera (amb un text i canviant la imatge), que la
        // reserva es antiga
        if((holder.mImageView.getDrawable().toString()).startsWith(
                "android.graphics.drawable.VectorDrawable")){
            holder.mTextSeient.setText("Antiga reserva");
        }

        // Si fas click a sobre del RelativeLayout sobre un dialeg de la
        // possició a eliminar si estàs d'acord
        holder.rLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obrim el dialeg
                //Log.d("position",String.valueOf(position));
                //Log.d("currentItemView.getCurrentReserva()",String.valueOf(currentItemView.getCurrentReserva()));
                obrirDialeg(position, v,currentItemView.getCurrentReserva());
            }
        });
    }

    @Override
    public int getItemCount() {

        return mExampleList.size();

    }

    /**
     * Obre un dialeg preguntant si vols eliminar el element clickat
     * @param position - int
     * @param v - View
     * @param currentBooking - Object of Booking
     */
    public void obrirDialeg(final int position, View v, final Booking currentBooking) {

        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext(),  R.style.AlertDialogTheme);

        builder.setTitle("Eliminar reserva");
        builder.setMessage(String.format("Estàs segur que vols eliminar la reserva feta del día %s ? " ,
                currentBooking.getBookingDate()));

        // Si acceptem passem a l'activitat anterior LlistarReservaActivity.class
        builder.setPositiveButton("Acceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                pos = position;

                eliminarReserva(currentBooking);

                Toast toast = Toast.makeText(mContext.getApplicationContext(), String.format("Dia " +
                        "%s s'ha eliminat de les reserves correctament" , currentBooking.getBookingDate()),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();

                //Intent
                Intent intent = new Intent(mContext, LlistarReservaActivity.class);
                mBundle.putString("elementMarcat", String.valueOf(position));
                intent.putExtras(mBundle);

                //el siguiente flag es para que podamos llamar a start activity sin errores
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        // Si rebutgem, canceles l'operació
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast toast = Toast.makeText(mContext.getApplicationContext(), "Operació cancelada",
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Elimina la reserva pasada com argument
     * @param bookingToDelete - Booking
     */
    public void eliminarReserva(Booking bookingToDelete) {
        HotspotApi request = new HotspotApi();
        request.deleteMyReserve(deleteBookCallBack, mAuthHelper.getIdToken(), bookingToDelete.getBookingDate());
    }

    /*
     * Resposta de trucada per a la eliminació de reserva de dies en l'oficina
     *  Depenent del missatge de sortida, enviarem un Toast per coneixer si s'ha
     *  fet correctament la reserva
     */
    private final NetworkRequest.Callback<String> deleteBookCallBack = new NetworkRequest.Callback<String>() {
        @Override
        public void onResponse(@NonNull String response) {
            Log.d(Constants.LOG_TAG, "Logout server response1:" + response);
            deleteMyBook(response);
            //dismissDialog();
        }

        @Override
        public void onError(String error) {
            Log.d("HOTSPOT_DEBUG2", "Logout server response error:" + error);
            //dismissDialog();
        }

        @Override
        public Class<String> type() {
            return String.class;
        }
    };

    /**
     * Estableix i elimina la meva reserva del JSON response
     * @param response
     */
    public void deleteMyBook(String response) {

        try {
            Log.d(Constants.LOG_TAG,"Data de la posicio a eliminar" +
                    myBooksArrayList.get(pos).getBookingDate());

            myBooksArrayList.remove(pos);

            Log.d(Constants.LOG_TAG, "Posicions totals:"+ String.valueOf
                    (myBooksArrayList.size()));

        } catch (Exception e) {
            Log.d("HOTSPOT_DEBUG", e.toString());
        }
    }


    public interface ClickListener {
        void onItemClick(int position, View v);

        void onItemLongClick(int position, View v);
    }



    /*
     * Mostra una gran quantitat de dades de forma ordenada i clara
     */
    public static class ExampleViewHolder extends RecyclerView.ViewHolder   implements View.OnClickListener, View.OnLongClickListener {

        public ImageView mImageView;
        public TextView mTextSeient;
        public TextView mTextReserva;
        public RelativeLayout rLayout;

        public ExampleViewHolder(@NonNull View itemView) {

            super(itemView);
            mImageView = itemView.findViewById(R.id.img1);
            mTextSeient = itemView.findViewById(R.id.ln1);
            mTextReserva = itemView.findViewById(R.id.ln2);
            rLayout = itemView.findViewById(R.id.rel1);

        }

        @Override
        public void onClick(View v) {

            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public class ClickReservaListener  implements View.OnClickListener{
        @Override
        public void onClick(View view) {

        }
    }

}
