package com.example.hotspots;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hotspots.DTO.Model;

/**
 * Vista detallada del Recycerview (amb els seus missatges informatius)
 * @author Francisco París
 * @version 03.2020
 * */
public class ViewDetails extends AppCompatActivity {

  Model model;
  ImageView imageDetails;
  TextView titleText, descriptionText, notificacio;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_view_details);

    imageDetails = (ImageView) findViewById(R.id.imageDetails);
    titleText = (TextView) findViewById(R.id.titleText);
    descriptionText = (TextView) findViewById(R.id.descriptionText);
    notificacio = (TextView)findViewById(R.id.notificacioText);


    if (getIntent() != null) {
      model = getIntent().getParcelableExtra("id");
      imageDetails.setImageResource(model.image);
      titleText.setText(model.title);
      textDescriptiu(model.description);
      notificacio.setText(model.notification);

    }
  }

  /*
   * Aquest text explica més detalladament la descripció del tipus
   * de notificació que ens dona i la seva explicació
   */
  public void textDescriptiu(String missatge){

    if(missatge.contains("Covid")){
      descriptionText.setText("° " + missatge + "\n"+ Constants.CONTACTE_POSITIU);
    }

    if(missatge.contains("error")){
      descriptionText.setText("° " + missatge + "\n"+ Constants.NOTIFICACIO_ERROR);
    }

    if(missatge.contains("reserva")){
      descriptionText.setText("° " + missatge + "\n"+ Constants.RECORDATORI);
    }

    if(missatge.contains("borrado")){
      descriptionText.setText("° " + missatge + "\n"+ Constants.CANCELACIO_RESERVA);
    }

  }
}