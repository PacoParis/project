package com.example.hotspots;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.auth0.android.jwt.JWT;
import com.example.hotspots.network.Token;

/**
 * Clau del nom d'usuari a la reclamació jwt
 * @author Francisco París
 * @version 01.2020
 */

public class AuthHelper {

    private static final String JWT_KEY_USERNAME = "user_id";
    private static final String JWT_KEY_NAME = "name";

    private static final String PREFS = "prefs";
    private static final String PREF_TOKEN = "pref_token";
    private static AuthHelper sInstance;
    private SharedPreferences mPrefs;

    private AuthHelper(@NonNull Context context) {
        mPrefs = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        sInstance = this;
    }

    public static AuthHelper getInstance(@NonNull Context context) {
        if (sInstance == null) {
            sInstance = new AuthHelper(context);
        }
        return sInstance;
    }

    @Nullable
    public String getIdToken() {
        return mPrefs.getString(PREF_TOKEN, null);
    }

    public void setIdToken(@NonNull Token token) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_TOKEN, token.getIdToken());
        editor.apply();
    }

    public boolean isLoggedIn() {
        String token = getIdToken();
        return token != null;
    }

    /**
     * Obté el nom d'usuari de l'usuari que ha iniciat la sessió
     * @return - nom d'usuari de l'usuari que ha iniciat la sessió
     */
    public String getUsername() {
        if (isLoggedIn()) {
            return decodeUsername(getIdToken());
        }
        return null;
    }

    @Nullable
    private String decodeUsername(String token) {
        JWT jwt = new JWT(token);
        try {
            if (jwt.getClaim(JWT_KEY_NAME) != null) {
                return jwt.getClaim(JWT_KEY_NAME).asString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getUserEmail() {
        if (isLoggedIn()) {
            return decodeUserEmail(getIdToken());
        }
        return null;
    }

    @Nullable
    private String decodeUserEmail(String token) {
        JWT jwt = new JWT(token);
        try {
            if (jwt.getClaim(JWT_KEY_NAME) != null) {
                return "email :" + jwt.getClaim(JWT_KEY_NAME).asString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void clear() {
        mPrefs.edit().clear().commit();
    }
}
