package com.example.hotspots;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hotspots.DTO.Booking;
import com.example.hotspots.DTO.Model;
import com.example.hotspots.DTO.NewsNotifications;
import com.example.hotspots.DTO.Notification;
import com.example.hotspots.DTO.Utils;
import com.example.hotspots.network.HotspotApi;
import com.example.hotspots.network.NetworkRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Clase que permet una intercació amb l'usuari, una vegada login, post fer un logout o
 * canviar la teva contrasenya
 * @author Francisco París
 * @version 03.2020
 * */
public class QuotesActivity extends AppCompatActivity implements View.OnClickListener, MenuItem.OnMenuItemClickListener {

    private TextView mTextWelcome;
    private EditText passwdAntic,passwdNou;
    private Button mButtonLogout, btnCanviPasswd, btnCanviPasswdDef, btnReserva,
                    btnLlistar, btnNotificacions;
    private FloatingActionButton fAB;
    private boolean oculta=true;
    private boolean ocultaBtns=true;

    private AuthHelper mAuthHelper;

    private ProgressDialog mProgressDialog;
    private NewsNotifications notifications;
    int pendingNotifications = 0;


    private LoginActivity loginActivity = new LoginActivity();
    private ArrayList<Booking> myBooks;
    private ArrayList<String> eachBook = new ArrayList<>();


    public static Intent getCallingIntent(Context context) {
        return new Intent(context, QuotesActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes);

        mTextWelcome = (TextView) findViewById(R.id.text_username);
        passwdAntic = (EditText)findViewById(R.id.etpasswd);
        passwdNou = (EditText)findViewById(R.id.etpasswdNova);
        mButtonLogout = (Button) findViewById(R.id.button_logout);
        mButtonLogout.setOnClickListener(this);
        btnCanviPasswd = (Button)findViewById(R.id.btn_cv_passwd);
        btnCanviPasswd.setOnClickListener(this);
        btnCanviPasswdDef = (Button)findViewById(R.id.btn_passwd_final);
        btnCanviPasswdDef.setOnClickListener(this);
        btnReserva=(Button)findViewById(R.id.btn_fer_reserva);
        btnReserva.setOnClickListener(this);
        btnLlistar=(Button)findViewById(R.id.btn_llistar);
        btnLlistar.setOnClickListener(this);
        btnNotificacions=(Button)findViewById(R.id.btn_notifications);
        btnNotificacions.setOnClickListener(this);

        fAB = (FloatingActionButton)findViewById(R.id.fAB_inf);
        fAB.setOnClickListener(this);

        ocultaTextContrasenya(oculta);

        mProgressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        mAuthHelper = AuthHelper.getInstance(this);

        // Truquem al token per veure les notificacions pendents que té
        HotspotApi request = new HotspotApi();

        request.showMyNotifications(notificationCallBack, mAuthHelper.getIdToken());

        notifications=new NewsNotifications(pendingNotifications);


        if (mAuthHelper.isLoggedIn()) {
            setupView();
            Log.d("My token", mAuthHelper.getIdToken());
        } else {
            finish();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menuItem = (MenuItem)menu.findItem(R.id.nav_notification);

        // check if any pending notification
        if (notifications.getContador() == 0) {
            // if no pending notification remove badge
            menuItem.setActionView(null);
        } else {

            // if notification than set the badge icon layout
            menuItem.setActionView(R.layout.notification_badge);
            // get the view from the nav item
            View view = menuItem.getActionView();
            // get the text view of the action view for the nav item
            badgeCounter = view.findViewById(R.id.badge_counter);
            // set the pending notifications value
            badgeCounter.setText(String.valueOf(notifications.getContador()));

            *//*badgeCounter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), NotificationsActivity.class);
                    startActivity(intent);
                }
            });*//*

        }

        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(getApplicationContext(), NotificationsActivity.class);
                startActivity(intent);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }*/

    /**
     * Oculta/Mostra alguns objectes
     * @params  oculta - boolean
     * */
    private void ocultaTextContrasenya(boolean oculta){
        if(oculta){
            passwdAntic.setVisibility(View.INVISIBLE);
            passwdNou.setVisibility(View.INVISIBLE);
            btnCanviPasswdDef.setVisibility(View.INVISIBLE);
        }else{
            passwdAntic.setVisibility(View.VISIBLE);
            passwdNou.setVisibility(View.VISIBLE);
            btnCanviPasswdDef.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Oculta/Mostra alguns objectes botons
     * @params  ocultaBtns - boolean
     * */
    private void ocultaBotons(boolean ocultaBtns){
        if(ocultaBtns){
            btnReserva.setVisibility(View.INVISIBLE);
            mButtonLogout.setVisibility(View.INVISIBLE);
            btnCanviPasswd.setVisibility(View.INVISIBLE);
            btnLlistar.setVisibility(View.INVISIBLE);
        }else{
            btnReserva.setVisibility(View.VISIBLE);
            mButtonLogout.setVisibility(View.VISIBLE);
            btnCanviPasswd.setVisibility(View.VISIBLE);
            btnLlistar.setVisibility(View.VISIBLE);
        }
    }


    private void setupView() {
        setWelcomeText(mAuthHelper.getUsername());
        //setWelcomeText(mAuthHelper.getUserEmail());
        //mButtonRandom.setOnClickListener(this);
        mButtonLogout.setOnClickListener(this);
    }

    /**
     * Estableix el text de benvinguda per a l'usuari que ha iniciat la sessió
     * @param username - nom d'usuari de l'usuari
     */
    private void setWelcomeText(String username) {
        //loginRequest = new LoginRequest();
        mTextWelcome.setText(String.format(getString(R.string.text_welcome), username + " "  ));
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            // Surt de la app
            case R.id.button_logout:
                HotspotApi request = new HotspotApi();
                Log.d("Token", mAuthHelper.getIdToken());
                request.doLogout(logoutCallBack, mAuthHelper.getIdToken());

                startActivity(LoginActivity.getCallingIntent(this));

                mAuthHelper.clear();
                // finish();
                return;

            // Sobre el editext dels password actual i nou i un bottó de canvi de password
            //Establim els edittext vuits, per si s'ha canviat la contrasenya en la mateixa
            // activitat, i ocultem els botons
            case R.id.btn_cv_passwd:
                passwdAntic.setText("");
                passwdNou.setText("");
                oculta=false;
                ocultaBtns=true;
                ocultaTextContrasenya(oculta);
                ocultaBotons(ocultaBtns);
                return;

            // Canvia de password si omples els dos camps necessaris
            // (antiga i nova contrasenya)
            case R.id.btn_passwd_final:
                if((passwdAntic.getText().toString().isEmpty() || passwdNou.getText().toString().isEmpty())){
                    Toast.makeText(getApplicationContext(),"Omple tots els camps", Toast.LENGTH_LONG).show();
                }else{
                    ocultaBtns=false;
                    ocultaBotons(ocultaBtns);
                    oculta=true;
                    ocultaTextContrasenya(oculta);
                    doChangePasswd();
                }
                return;

            // Inicia la possibilitat de fer una reserva
            case R.id.btn_fer_reserva:
                Intent i = new Intent(getApplicationContext(), CalendariActivity.class);
                startActivity(i);
                return;

            // Reprodueix un llistat de les reserves
            case R.id.btn_llistar:
                //mostrarReserva();
                Intent i2 = new Intent(getApplicationContext(), LlistarReservaActivity.class);
                startActivity(i2);
                return;

            case R.id.fAB_inf:
                Intent i3 = new Intent(getApplicationContext(), InfoActivity.class);
                startActivity(i3);
                return;

            case R.id.btn_notifications:
                Intent i4 = new Intent(getApplicationContext(), NotificationsActivity.class);
                i4.putExtra("notificacionsPendents", pendingNotifications);
                startActivity(i4);
                return;

        }

    }


    /*
     * Canvia el password agafant la contrasenya antiga i la nova
     */
    private void doChangePasswd() {
        String anticPasswd = getPasswdAntic();
        String nouPasswd = getPasswdNou();

        if (TextUtils.isEmpty(anticPasswd) || TextUtils.isEmpty(nouPasswd)) {
            Toast.makeText(this, R.string.toast_no_empty_field, Toast.LENGTH_SHORT).show();
            return;
        }

        mProgressDialog.setMessage(getString(R.string.progress_ch_pswd));
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();

        HotspotApi request = new HotspotApi();
        request.changePassword(changePasswordCallBack,mAuthHelper.getIdToken(),nouPasswd, anticPasswd);
    }

    public String getPasswdAntic() {
        return passwdAntic.getText().toString().trim();
    }

    public String getPasswdNou() {
        return passwdNou.getText().toString().trim();
    }

    /*
     * Mètode de trucada a la sortida de la sessió
     * */
    private NetworkRequest.Callback<String> logoutCallBack = new NetworkRequest.Callback<String>() {
        @Override
        public void onResponse(@NonNull String response) {
            Log.d(Constants.LOG_TAG, "Logout server response:" +  response);
            // deseu el testimoni i aneu a la pàgina de perfil
            //Toast.makeText(QuotesActivity.this, "Ok logout", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(String error) {
            Log.d("HOTSPOT_DEBUG1", "Logout server response error:" +  error);
            Toast.makeText(QuotesActivity.this, error, Toast.LENGTH_SHORT).show();
        }

        @Override
        public Class<String> type() {
            return String.class;
        }
    };

    /*
     * Veiem les sortides de resposta diferents que ens dona a l'hora de canviar
     * la contrasenya antiga
     * */
    private NetworkRequest.Callback<String> changePasswordCallBack = new NetworkRequest.Callback<String>() {
        @Override
        public void onResponse(@NonNull String response) {
            Log.d(Constants.LOG_TAG, "Logout server response:" +  response);

            // deseu el testimoni i aneu a la pàgina de perfil
            if(response.contains("Incorrect oldPassword")){
                Toast.makeText(QuotesActivity.this, getString(R.string.bad_old_password) , Toast.LENGTH_SHORT).show();
            }
            if(response.contains("Password changed!!")){
                Toast.makeText(QuotesActivity.this, getString(R.string.right_old_password) , Toast.LENGTH_SHORT).show();
            }
            if(response.contains("Token is expired")){
                Toast.makeText(QuotesActivity.this, getString(R.string.error_token_password) , Toast.LENGTH_SHORT).show();
            }

            dismissDialog();
        }

        @Override
        public void onError(String error) {
            Log.d("HOTSPOT_DEBUG2", "Logout server response error:" +  error);
            //Toast.makeText(QuotesActivity.this, error, Toast.LENGTH_SHORT).show();
            dismissDialog();
        }

        @Override
        public Class<String> type() {
            return String.class;
        }
    };


    /**
     * Omet el diàleg si es mostra
     * */
    private void dismissDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }



    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_notification:
                Toast.makeText(getApplicationContext(),"Hola",Toast.LENGTH_LONG).show();
                return true;
            case R.id.badge_counter:
                Toast.makeText(getApplicationContext(),"Hola",Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    /*
     * Resposta de trucada per a la llista de notificacions
     *  Depenent del missatge de sortida, enviarem un Toast per coneixer si s'ha
     *  fet correctament la reserva
     * */
    private final NetworkRequest.Callback<String> notificationCallBack = new NetworkRequest.Callback<String>() {
        @Override
        public void onResponse(@NonNull String response) {
            Log.d(Constants.LOG_TAG, "Logout server response 1:" + response);
            getMyNotifications(response);
            dismissDialog();
        }

        @Override
        public void onError(String error) {
            Log.d("HOTSPOT_DEBUG2", "Logout server response error:" + error);
            dismissDialog();
        }

        @Override
        public Class<String> type() {
            return String.class;
        }
    };

    /**
     * Obté totes les meves notificacions from JSON response
     * @param response - String
     */
    public int getMyNotifications(String response) {

        try {
            JSONArray jsonArray = new JSONArray(response);

            com.example.hotspots.DTO.Notification currentNotification;

            for (int i = 0; i < jsonArray.length(); i++){

                currentNotification = (com.example.hotspots.DTO.Notification) Utils.buildObjectFromResponse(jsonArray.get(i).toString(),Notification.class);

                // Controlem quantes notificacions pendents de veure tens
                if((currentNotification.getIsNew().contains("true"))){

                    pendingNotifications+=1;

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return pendingNotifications;
    }


}
